﻿using BepInEx;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace RaidDefense
{
    [BepInPlugin("com.angrychild.raidheim", "Raidheim", "1.1.2")]
    public class Patcher : BaseUnityPlugin
    {
        const string RAIDS_DATA_FILE_EXTENTION = ".raidsdata";
        public static string PluginPath { get; private set; }

        private Harmony harmony = new Harmony("com.angrychild.raidheim");

        public static RaidDefenceSystem mainSystem = new RaidDefenceSystem();

        private void Awake()
        {
            harmony.PatchAll();
            PluginPath = Path.GetDirectoryName(Info.Location);
        }

        [HarmonyPatch(typeof(Chat), "OnNewChatMessage")]
        private class OnMessagePatch
        {
            static void Prefix(GameObject go, long senderID, Vector3 pos, Talker.Type type, string user, string text)
            {
                if (ZNet.instance != null && ZNet.instance.IsServer())
                {
                    mainSystem.OnReceiveChatMessage(type, user, text);
                }
            }
        }

        [HarmonyPatch(typeof(Character), "OnDeath")]
        private class OnCharacterDeathPatch
        {
            static void Prefix(Character __instance)
            {
                if (ZNet.instance != null && ZNet.instance.IsServer())
                {
                    mainSystem.OnCharacterDeathServer(__instance);
                }
            }
        }

        [HarmonyPatch(typeof(RandEventSystem), "GetPossibleRandomEvents")]
        private class DisableRandomEventsPatch
        {
            static void Postfix(ref List<KeyValuePair<RandomEvent, Vector3>> __result)
            {
                __result = new List<KeyValuePair<RandomEvent, Vector3>>();
            }
        }

        [HarmonyPatch(typeof(Minimap), "Awake")]
        private class MinimapAwakePatch
        {
            static void Postfix(List<Minimap.PinData> ___m_pins)
            {
                mainSystem.MinimapPins = ___m_pins;
            }
        }

        [HarmonyPatch(typeof(ZDOMan), "Load")]
        private class ZDOManLoadPatch
        {
            static void Postfix(Dictionary<ZDOID, ZDO> ___m_objectsByID)
            {
                mainSystem.ZDOByID = ___m_objectsByID;
            }
        }

        [HarmonyPatch(typeof(ZNet), "Update")]
        private class UpdatePatch
        {
            static void Prefix(ZNet __instance)
            {
                if (__instance.IsServer())
                {
                    mainSystem.ServerUpdate();
                }
            }
        }

        [HarmonyPatch(typeof(ZNetScene), "CreateObject")]
        private class CreateObjectPatch
        {
            static void Postfix(GameObject __result, ZDO zdo)
            {
                // This is how you can find relevant prefab ids
                //Debug.Log(zdo.GetPrefab() + "  " + __result?.name);
            }
        }

        [HarmonyPatch(typeof(World), nameof(World.RemoveWorld))]
        private class WorldRemovePatch
        {
            static void Postfix(string name)
            {
                try
                {
                    string text = Utils.GetSaveDataPath() + "/worlds/" + name;
                    File.Delete(text + RAIDS_DATA_FILE_EXTENTION);
                }
                catch (Exception ex)
                {
                    Debug.LogError("Failed to delete world raid data - " + name);
                }
            }
        }

        [HarmonyPatch(typeof(Player), nameof(Player.OnSpawned))]
        private class StaticDataDebugPatch
        {
            static void Postfix()
            {
                // Decent for debugging static data
            }
        }

        [HarmonyPatch(typeof(EnvMan), "GetEnvironmentOverride")]
        private class EnvironmentPatch
        {
            static void Postfix(ref string __result)
            {
                if (__result == null)
                {
                    __result = mainSystem.GetEnvironmentOverride();
                }
            }
        }

        [HarmonyPatch(typeof(ZNet), "SaveWorld")]
        private class SavePatch
        {
            static void Postfix(World ___m_world)
            {
                mainSystem.Save(GetSavePath(___m_world));
            }
        }

        [HarmonyPatch(typeof(ZNet), "LoadWorld")]
        private class LoadPatch
        {
            static void Postfix(World ___m_world)
            {
                fastJSON.JSON.Parameters.UseFastGuid = false;
                fastJSON.JSON.Parameters.UseExtensions = false;

                mainSystem.Load(GetSavePath(___m_world));
            }
        }

        [HarmonyPatch(typeof(BaseAI), nameof(BaseAI.IsEnemy))]
        private class IsEnemyPatch
        {
            static void Postfix(Character a, Character b, ref bool __result)
            {
                Character.Faction f1 = a.GetFaction();
                Character.Faction f2 = b.GetFaction();

                if (f1 == f2) return;

                if (f1 == Character.Faction.Players || f2 == Character.Faction.Players)
                {
                    if (a.IsTamed() || b.IsTamed())
                    {
                        __result = false;
                    }
                    else
                    {
                        __result = true;
                    }
                }
                else
                {
                    __result = false;
                }
            }
        }

        private static string GetSavePath(World world)
        {
            return Path.ChangeExtension(world.GetDBPath(), RAIDS_DATA_FILE_EXTENTION);
        }
    }
}
