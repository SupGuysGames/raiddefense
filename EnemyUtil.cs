﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidDefense
{
    public class EnemyUtil
    {
        private static Dictionary<int, EnemyType> prefIdToEnemy = new Dictionary<int, EnemyType>();
        private static Dictionary<EnemyType, int> enemyToPrefID = new Dictionary<EnemyType, int>();

        public enum EnemyType
        {
            Boar = 0,
            Neck = 1,
            Greyling = 2,
            Greydwarf = 3,
            GreydwarfBrute = 4,
            GreydwarfShaman = 5,
            Troll = 6,
            Ghost = 7,
            Skeleton = 8,
            RancidRemains = 9,
            Blob = 10,
            Oozer = 11,
            Draugr = 12,
            DraugrElite = 13,
            Leech = 14,
            Surtling = 15,
            Wraith = 16,
            Wolf = 17,
            Drake = 18,
            StoneGolem = 19,
            Fenring = 20,
            Deathsquito = 21,
            Lox = 22,
            FulingSword = 23,
            FulingSpear = 24,
            FulingBerseker = 25,
            FulingShaman = 26,
            Serpent = 27,
            Eikthyr = 28,
            Elder = 29,
            Bonemass = 30,
            Moder = 31,
            Yagluth = 32
        }

        static EnemyUtil()
        {
            prefIdToEnemy = new Dictionary<int, EnemyType>
            {
                { -1670867714, EnemyType.Boar           },
                { -2081058657, EnemyType.Neck           },
                { -50960667, EnemyType.Greyling         },
                { 1126707611, EnemyType.Greydwarf       },
                { -1374218359, EnemyType.GreydwarfBrute },
                { 762782418, EnemyType.GreydwarfShaman  },
                { 425751481, EnemyType.Troll            },
                { -696918929, EnemyType.Ghost           },
                { -1035090735, EnemyType.Skeleton       },
                { -1723003302, EnemyType.RancidRemains  },
                { 829393023, EnemyType.Blob             },
                { -249968000, EnemyType.Oozer           },
                { 505464631, EnemyType.Draugr           },
                { 437915453, EnemyType.DraugrElite      },
                { -1537236269, EnemyType.Leech          },
                { 1370511288, EnemyType.Surtling        },
                { 68955605, EnemyType.Wraith            },
                { 1010961914, EnemyType.Wolf            },
                { -729507656, EnemyType.Drake           },
                { 1814827443, EnemyType.StoneGolem      },
                { 1388670159, EnemyType.Fenring         },
                { -1609638819, EnemyType.Deathsquito    },
                { 1502599715, EnemyType.Lox             },
                { -137741679, EnemyType.FulingSword     },
                { -1508843442, EnemyType.FulingSpear    },
                { -939999423, EnemyType.FulingBerseker  },
                { -315180887, EnemyType.FulingShaman    },
                { 1671717323, EnemyType.Serpent         },
                { -938588874, EnemyType.Eikthyr         },
                { -2101309657, EnemyType.Elder          },
                { -146537656, EnemyType.Bonemass        },
                { -408509179, EnemyType.Moder           },
                {  -221799126, EnemyType.Yagluth        }
            };

            enemyToPrefID = new Dictionary<EnemyType, int>();
            foreach (var kv in prefIdToEnemy)
            {
                enemyToPrefID.Add(kv.Value, kv.Key);
            }
        }

        public static int GetID(EnemyType type)
        {
            if (enemyToPrefID.TryGetValue(type, out int v)) return v;
            return 0;
        }

        public static EnemyType GetEnemy(int id)
        {
            if (prefIdToEnemy.TryGetValue(id, out EnemyType v)) return v;
            return 0;
        }
    }
}
