﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaidDefense
{
    public class Location
    {
        public LocationType Type { get; set; }
        public Vector3 Position { get; set; }
    }

    public enum LocationType
    {
        EikthyrAltar,
        ElderAltar,
        BonemassAltar,
        ModerAltar,
        YagluthAltar
    }
}
