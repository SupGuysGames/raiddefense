﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidDefense
{
    public class SaveData
    {
        static string[] KEY_VAR_SEPARATOR = new string[] { " " };
        static CultureInfo CULTURE = new CultureInfo("es-ES");

        public int CURRENT_LEVEL = 0;
        public int CURRENT_WAVE = 0;
        public int NEXT_EVENT_TIME = 0;
        public bool IS_FIGHTING = false;
        public bool LOST_LAST_RAID = false;
        public bool TIMER_RUNNING = true;
        public string CONFIG_NAME = "";

        public List<EnemyGroup> SPAWNED = new List<EnemyGroup>();
        public List<EnemyGroup> KILLED = new List<EnemyGroup>();

        public bool IsInitialized()
        {
            return CURRENT_LEVEL != 0 && CURRENT_WAVE != 0;
        }

        public void Load(string path)
        {
            SPAWNED.Clear();
            KILLED.Clear();
            CURRENT_LEVEL = 0;
            CURRENT_WAVE = 0;
            NEXT_EVENT_TIME = 0;
            IS_FIGHTING = false;
            LOST_LAST_RAID = false;
            TIMER_RUNNING = true;
            CONFIG_NAME = "default";

            if (!File.Exists(path)) return;

            foreach (string line in File.ReadAllLines(path))
            {
                string[] parts = line.Split(KEY_VAR_SEPARATOR, StringSplitOptions.RemoveEmptyEntries);

                if (parts.Length < 2) return;

                try
                {
                    if (parts[0].Trim().Equals(nameof(CURRENT_LEVEL)))
                    {
                        CURRENT_LEVEL = int.Parse(parts[1], CULTURE);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(CURRENT_WAVE)))
                    {
                        CURRENT_WAVE = int.Parse(parts[1], CULTURE);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(NEXT_EVENT_TIME)))
                    {
                        NEXT_EVENT_TIME = int.Parse(parts[1], CULTURE);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(IS_FIGHTING)))
                    {
                        IS_FIGHTING = bool.Parse(parts[1]);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(LOST_LAST_RAID)))
                    {
                        LOST_LAST_RAID = bool.Parse(parts[1]);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(TIMER_RUNNING)))
                    {
                        TIMER_RUNNING = bool.Parse(parts[1]);
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(SPAWNED)) && parts.Length == 4)
                    {
                        SPAWNED.Add(ParseEnemyGroup(parts));
                    }
                }
                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(KILLED)) && parts.Length == 4)
                    {
                        KILLED.Add(ParseEnemyGroup(parts));
                    }
                }

                catch { }

                try
                {
                    if (parts[0].Trim().Equals(nameof(CONFIG_NAME)))
                    {
                        CONFIG_NAME = parts[1].Trim();
                    }
                }

                catch { }
            }
        }

        private EnemyGroup ParseEnemyGroup(string[] parts)
        {
            return new EnemyGroup
            {
                enemy = (EnemyUtil.EnemyType)int.Parse(parts[1], CULTURE),
                stars = int.Parse(parts[2], CULTURE),
                count = int.Parse(parts[3], CULTURE)
            };
        }

        private string ToSaveLine(string title, EnemyGroup g)
        {
            return title + " " + ((int)g.enemy) + " " + g.stars + " " + g.count;
        }

        public void Save(string path)
        {
            string result = nameof(CURRENT_LEVEL) + " " + CURRENT_LEVEL.ToString(CULTURE) + Environment.NewLine +
                nameof(CURRENT_WAVE) + " " + CURRENT_WAVE.ToString(CULTURE) + Environment.NewLine +
                nameof(NEXT_EVENT_TIME) + " " + NEXT_EVENT_TIME.ToString(CULTURE) + Environment.NewLine +
                nameof(LOST_LAST_RAID) + " " + LOST_LAST_RAID.ToString(CULTURE) + Environment.NewLine +
                nameof(TIMER_RUNNING) + " " + TIMER_RUNNING.ToString(CULTURE) + Environment.NewLine +
                nameof(IS_FIGHTING) + " " + IS_FIGHTING.ToString(CULTURE) + Environment.NewLine +
                nameof(CONFIG_NAME) + " " + CONFIG_NAME + Environment.NewLine;

            foreach (EnemyGroup enemyGroup in SPAWNED)
            {
                result += ToSaveLine(nameof(SPAWNED), enemyGroup) + Environment.NewLine;
            }

            foreach (EnemyGroup enemyGroup in KILLED)
            {
                result += ToSaveLine(nameof(KILLED), enemyGroup) + Environment.NewLine;
            }

            File.WriteAllText(path, result);
        }
    }
}
