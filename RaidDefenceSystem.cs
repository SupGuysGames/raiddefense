﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaidDefense
{
    public class RaidDefenceSystem
    {
        const string RAIDS_SYSTEM_CREATURE = "raids-system-creature";

        const int BOSS_ZONE_EIKTHYR = -316818231;
        const int BOSS_ZONE_ELDER = 1828406738;
        const int BOSS_ZONE_BONEMASS = -146537656;
        const int BOSS_ZONE_DRAGONQUEEN = 1225607547;
        const int BOSS_ZONE_YAGLUTH = -221799126;

        const string CONFIG_FILE_EXTENSION = ".raidsconfig";

        public Dictionary<ZDOID, ZDO> ZDOByID { get; set; } = new Dictionary<ZDOID, ZDO>();
        public List<Minimap.PinData> MinimapPins { get; set; } = new List<Minimap.PinData>();
        private List<Location> _relevantLocations { get; set; } = new List<Location>();
        private Vector3 _nextEventLocation = Vector3.zero;
        private int _nextEventLocationLevel = -1;
        private bool cllcLoaded = false;

        private int lastHelperSpawn = 0;
        private int lastGoalSpawn = 0;

        private SaveData save = new SaveData();
        private Config config = new Config();

        public void ServerUpdate()
        {
            // Lets ensure we have everything loaded that we need for the system to work
            if (Chat.instance == null ||
                Minimap.instance == null ||
                ZoneSystem.instance == null ||
                ZNet.instance == null ||
                ZNetScene.instance == null ||
                ZRoutedRpc.instance == null) return;

            if (((int)(Time.time)) != ((int)((Time.time - Time.deltaTime))))
            {
                ServerUpdate_1000ms();
            }

            if (((int)(Time.time / 0.1f)) != ((int)((Time.time - Time.deltaTime) / 0.1f)))
            {
                ServerUpdate_100ms();
            }

            ServerUpdate_0ms();
        }

        private void ServerUpdate_0ms()
        {

        }

        private void ServerUpdate_100ms()
        {
            if (save.IsInitialized() && save.IS_FIGHTING)
            {
                EnemySpawnUpdate();
            }
        }

        private void ServerUpdate_1000ms()
        {
            // Check if initialized game
            if (!save.IsInitialized()) return;

            if (!save.TIMER_RUNNING) return;

            // Tick time
            if (ZNet.instance.GetNrOfPlayers() == 0)
            {
                return;
            }

            save.NEXT_EVENT_TIME -= 1;

            if (SecondsLeft > 0)
            {
                SecondsTick();
            }
            else
            {
                SecondsExpired();
            }
        }

        private void SecondsTick()
        {
            if (save.IS_FIGHTING)
            {
                if (IsCurrentWaveBeaten())
                {
                    SecondsExpired();
                }
                else
                {
                    ShoutSystemText("Kill " + GetRemainingGoalEnemies() + " more of " +
                        CurrentGoalGroup.enemy.ToString() + GetStars(CurrentGoalGroup.stars),
                        false);
                }
            }
            else
            {
                if (save.LOST_LAST_RAID)
                {
                    ShoutSystemText("Raid lost, prepare for another attempt");
                }
                else
                {
                    if (save.CURRENT_WAVE == 1)
                    {
                        ShoutSystemText("Prepare for next raid");
                    }
                    else
                    {
                        ShoutSystemText("Prepare for next wave");
                    }
                }

                if (save.CURRENT_WAVE == 1)
                {
                    if (SecondsLeft == 4 * 60 * 60) SendMiddleHudMessage("4 Hours Until Raid Begins");
                    if (SecondsLeft == 2 * 60 * 60) SendMiddleHudMessage("3 Hours Until Raid Begins");
                    if (SecondsLeft == 2 * 60 * 60) SendMiddleHudMessage("2 Hours Until Raid Begins");
                    if (SecondsLeft == 1 * 60 * 60) SendMiddleHudMessage("1 Hour Until Raid Begins");
                    if (SecondsLeft == 30 * 60) SendMiddleHudMessage("30 Minutes Until Raid Begins");
                    if (SecondsLeft == 10 * 60) SendMiddleHudMessage("10 Minutes Until Raid Begins");
                    if (SecondsLeft == 10) SendMiddleHudMessage("10 Seconds Until Raid Begins");
                }

                if (SecondsLeft == 3) SendMiddleHudMessage("3");
                if (SecondsLeft == 2) SendMiddleHudMessage("2");
                if (SecondsLeft == 1) SendMiddleHudMessage("1");
            }
        }

        public string GetStars(int n)
        {
            string result = "";
            for (int i = 0; i < n; i++)
            {
                result += "★";
            }
            return result;
        }

        private void SecondsExpired()
        {
            save.LOST_LAST_RAID = false;

            if (!save.IS_FIGHTING)
            {
                save.IS_FIGHTING = true;
                save.NEXT_EVENT_TIME = CurrentWave.durationSeconds;

                SendMiddleHudMessage("Kill " + CurrentWave.goalGroup.count + " " +
                        CurrentGoalGroup.enemy.ToString() + GetStars(CurrentGoalGroup.stars));
            }
            else
            {
                save.IS_FIGHTING = false;

                if (IsCurrentWaveBeaten())
                {
                    if (IsLastWaveOfLevel)
                    {
                        save.CURRENT_LEVEL++;
                        if (save.CURRENT_LEVEL > config.PredefinedRaids.Length) save.CURRENT_LEVEL = config.PredefinedRaids.Length;

                        save.CURRENT_WAVE = 1;
                        save.NEXT_EVENT_TIME = config.GetSecondsToLevel(save.CURRENT_LEVEL);
                        SendMiddleHudMessage("You Have Beaten Raid #" + (save.CURRENT_LEVEL - 1) + "!");
                    }
                    else
                    {
                        save.CURRENT_WAVE++;
                        save.NEXT_EVENT_TIME = config.WaveCooldown;
                        SendMiddleHudMessage((save.CURRENT_WAVE - 1) + "/" + CurrentRaid.Waves.Length + " waves cleared");
                    }
                }
                else
                {
                    save.CURRENT_WAVE = 1;
                    save.NEXT_EVENT_TIME = config.LoseLevelCooldown;
                    save.LOST_LAST_RAID = true;
                    SendMiddleHudMessage("Failure");
                }
            }

            save.KILLED.Clear();
            save.SPAWNED.Clear();
            DespawnAllRaidsCreatures();
        }

        private void EnemySpawnUpdate()
        {
            // Spawn main goal group
            if (Math.Abs(SecondsLeft - lastGoalSpawn) > CurrentWave.goalUnitSpawnDeltaSeconds)
            {
                bool spawned = false;

                foreach (EnemyGroup spawnedGroup in save.SPAWNED)
                {
                    if (CurrentGoalGroup.enemy == spawnedGroup.enemy && CurrentGoalGroup.stars == spawnedGroup.stars && spawnedGroup.count >= CurrentGoalGroup.count)
                    {
                        spawned = true;
                        break;
                    }
                }

                if (!spawned)
                {
                    if (Spawn1(CurrentGoalGroup) || Spawn1(CurrentGoalGroup) || Spawn1(CurrentGoalGroup))
                    {
                        lastGoalSpawn = SecondsLeft;
                        Add(save.SPAWNED, CurrentGoalGroup.enemy, CurrentGoalGroup.stars);
                    }
                }
            }

            // Spawn helpers
            if (Math.Abs(SecondsLeft - lastHelperSpawn) > CurrentWave.helperWaveSpawnDeltaSeconds)
            {
                lastHelperSpawn = SecondsLeft;

                foreach (EnemyGroup targetGroup in CurrentWave.helperGroups)
                {
                    int sp = 0;
                    for (int i = 0; i < targetGroup.count * 3 && sp < targetGroup.count; i++)
                    {
                        if (Character.GetAllCharacters().Count >= config.GameCharacterSoftCap) return;

                        if (Spawn1(targetGroup))
                        {
                            sp++;
                        }
                    }
                }
            }
        }

        private void SendMiddleHudMessage(string message)
        {
            MessageHud.instance.MessageAll(MessageHud.MessageType.Center, message);
        }

        private void SendTopLeftHudMessage(string message)
        {
            MessageHud.instance.MessageAll(MessageHud.MessageType.TopLeft, message);
        }

        private void ShoutSystemText(string text, bool includeHours = true)
        {
            SendChatMessage(NextEventPosition + Vector3.up * 2f, Talker.Type.Shout, GetTimeText(SecondsLeft, includeHours), text);
        }

        private void SendChatMessage(Vector3 position, Talker.Type type, string title, string text)
        {
            if (ZRoutedRpc.instance == null) return;
            ZRoutedRpc.instance.InvokeRoutedRPC(ZRoutedRpc.Everybody, "ChatMessage", position, (int)type, title, text);
        }

        public void OnReceiveChatMessage(Talker.Type ctype, string user, string text)
        {
            if (text.Trim().ToLower().Equals("/raids init"))
            {
                if (!save.IsInitialized())
                {
                    InitializeNewGame();
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Initialized raids game");
                }
            }
            else if (text.Trim().ToLower().StartsWith("/raids skip"))
            {
                try
                {
                    int seconds = int.Parse(text.ToLower().Replace("/raids skip", "").Trim());
                    save.NEXT_EVENT_TIME -= seconds;
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Skipped " + seconds + " seconds");
                }
                catch { }
            }
            else if (text.Trim().ToLower().StartsWith("/raids delay"))
            {
                try
                {
                    int seconds = int.Parse(text.ToLower().Replace("/raids delay", "").Trim());
                    save.NEXT_EVENT_TIME += seconds;
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Added " + seconds + " seconds");
                }
                catch { }
            }
            else if (text.Trim().ToLower().Equals("/raids resume"))
            {
                if (save.IsInitialized())
                {
                    save.TIMER_RUNNING = true;
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Timer resumed");
                }
            }
            else if (text.Trim().ToLower().Equals("/raids pause"))
            {
                if (save.IsInitialized())
                {
                    save.TIMER_RUNNING = false;
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Timer paused");
                }
            }
            else if (text.Trim().StartsWith("/raids config"))
            {
                string c = text.Replace("/raids config", "").Trim();

                if (c.Length == 0)
                {
                    SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Current config: " + save.CONFIG_NAME);
                }
                else
                {
                    save.CONFIG_NAME = c;
                    LoadConfig(save);
                }
            }
        }

        public string GetEnvironmentOverride()
        {
            //if (save.IS_FIGHTING || SecondsLeft < config.ChangeEnvironmentBeforeSeconds)
            //{
            //    // Only works on server PepeHands
            //    //return "nofogts";
            //}

            return null;
        }

        private bool Spawn1(EnemyGroup type)
        {
            Vector2 dir = UnityEngine.Random.insideUnitCircle.normalized;
            dir *= UnityEngine.Random.Range(config.SpawnInnerRadius, config.SpawnOuterRadius);
            Vector3 spawnPoint = NextEventPosition + new Vector3(dir.x, 0f, dir.y);

            if (IsSpawnPointGood(ref spawnPoint))
            {
                return Spawn1(type, spawnPoint);
            }

            return false;
        }

        private bool Spawn1(EnemyGroup type, Vector3 spawnPoint)
        {
            GameObject prefab = ZNetScene.instance.GetPrefab(EnemyUtil.GetID(type.enemy));
            if (prefab == null)
            {
                Debug.LogWarning("Unable to find prefab for " + type.enemy);
                return false;
            }

            GameObject gameObject = UnityEngine.Object.Instantiate(prefab, spawnPoint, Quaternion.identity);
            BaseAI component = gameObject.GetComponent<BaseAI>();
            if (!(component != null))
            {
                return false;
            }

            component.SetPatrolPoint(NextEventPosition);
            component.SetHuntPlayer(hunt: true);

            Character component2 = gameObject.GetComponent<Character>();

            if (type.stars > 0)
            {
                int stars = cllcLoaded ? type.stars : Math.Min(type.stars, 2);
                
                if ((bool)component2)
                {
                    component2.SetLevel(stars + 1);
                }
            }

            if (cllcLoaded && component2 != null)
            {
                CreatureLevelControl.API.SetAffixBoss(component2, type.bossAffix);
                CreatureLevelControl.API.SetExtraEffectCreature(component2, type.extraEffect);
                CreatureLevelControl.API.SetInfusionCreature(component2, type.creatureInfusion);
            }

            MonsterAI monsterAI = component as MonsterAI;
            if ((bool)monsterAI)
            {
                //    if (!critter.m_spawnAtDay)
                //    {
                //        monsterAI.SetDespawnInDay(despawn: true);
                //    }
                //    if (eventSpawner)
                //    {
                //        monsterAI.SetEventCreature(despawn: true);
                //    }
                monsterAI.SetDespawnInDay(despawn: false);
                monsterAI.SetEventCreature(despawn: false);
            }
            
            ZNetView view = gameObject.GetComponent<ZNetView>();
            if (view != null)
            {
                if (view.IsOwner() && view.GetZDO() != null)
                {
                    view.GetZDO().Set(RAIDS_SYSTEM_CREATURE, true);
                }
            }

            return true;
        }

        private void DespawnAllRaidsCreatures()
        {
            foreach (Character c in Character.GetAllCharacters())
            {
                ZNetView view = c.GetComponent<ZNetView>();
                if (view != null)
                {
                    if (view.IsOwner() && view.GetZDO() != null)
                    {
                        if (view.GetZDO().GetBool(RAIDS_SYSTEM_CREATURE, false))
                        {
                            view.Destroy();
                        }
                    }
                }
            }
        }

        private bool IsSpawnPointGood(ref Vector3 spawnPoint)
        {
            ZoneSystem.instance.GetGroundData(ref spawnPoint, out var normal, out var biome, out var biomeArea, out var hmap);
            if (ZoneSystem.instance.IsBlocked(spawnPoint))
            {
                return false;
            }

            // Lets not spawn on steep hills
            float num2 = Mathf.Cos((float)Math.PI / 180f * 30);
            float num3 = Mathf.Cos((float)Math.PI / 180f * 0);
            if (normal.y < num2 || normal.y > num3)
            {
                return false;
            }

            if ((bool)EffectArea.IsPointInsideArea(spawnPoint, EffectArea.Type.PlayerBase))
            {
                return false;
            }

            return true;
        }

        public void OnCharacterDeathServer(Character character)
        {
            if (!save.IS_FIGHTING) return;

            ZNetView component = character.GetComponent<ZNetView>();
            if (component != null && component.GetZDO() != null)
            {
                EnemyUtil.EnemyType e = EnemyUtil.GetEnemy(component.GetZDO().GetPrefab());
                int stars = character.GetLevel() - 1;

                Add(save.KILLED, e, stars);

                if (CurrentWave.goalGroup.enemy == e && CurrentWave.goalGroup.stars == stars)
                {
                    int rem = GetRemainingGoalEnemies();
                    if (rem != 0)
                    {
                        SendTopLeftHudMessage(
                        GetRemainingGoalEnemies() + " " +
                        CurrentGoalGroup.enemy.ToString() + GetStars(CurrentGoalGroup.stars) + " left");
                    }
                }
            }
        }

        private void Add(List<EnemyGroup> target, EnemyUtil.EnemyType e, int stars)
        {
            foreach (EnemyGroup g in target)
            {
                if (g.enemy == e && g.stars == stars)
                {
                    g.count++;
                    return;
                }
            }

            target.Add(new EnemyGroup { enemy = e, stars = stars, count = 1 });
        }

        private int GetRemainingGoalEnemies()
        {
            foreach (EnemyGroup saveGroup in save.KILLED)
            {
                if (CurrentGoalGroup.enemy == saveGroup.enemy && CurrentGoalGroup.stars == saveGroup.stars)
                {
                    return Mathf.Max(0, CurrentGoalGroup.count - saveGroup.count);
                }
            }

            return CurrentGoalGroup.count;
        }

        private string GetTimeText(int seconds, bool showHours = true)
        {
            if (seconds < 0) seconds = 0;

            TimeSpan t = TimeSpan.FromSeconds(seconds);

            if (showHours)
            {
                return string.Format("{0:D2}:{1:D2}:{2:D2}", t.Hours, t.Minutes, t.Seconds);
            }
            else
            {
                return string.Format("{0:D2}:{1:D2}", t.Minutes, t.Seconds);
            }
        }

        private bool IsCurrentWaveBeaten()
        {
            foreach (EnemyGroup saveGroup in save.KILLED)
            {
                if (CurrentGoalGroup.enemy == saveGroup.enemy && CurrentGoalGroup.stars == saveGroup.stars && saveGroup.count >= CurrentGoalGroup.count)
                {
                    return true;
                }
            }

            return false;
        }

        private EnemyGroup CurrentGoalGroup
        {
            get { return CurrentWave.goalGroup; }
        }

        private EnemyRaid CurrentRaid
        {
            get
            {
                int levelId = save.CURRENT_LEVEL - 1;
                if (levelId >= config.PredefinedRaids.Length) levelId = config.PredefinedRaids.Length - 1;

                if (levelId < 0) return config.PredefinedRaids[0];

                return config.PredefinedRaids[levelId];
            }
        }

        private EnemyWave CurrentWave
        {
            get
            {
                int levelId = save.CURRENT_LEVEL - 1;
                if (levelId >= config.PredefinedRaids.Length) levelId = config.PredefinedRaids.Length - 1;

                int waveId = save.CURRENT_WAVE - 1;
                if (waveId > config.PredefinedRaids[levelId].Waves.Length) waveId = config.PredefinedRaids[levelId].Waves.Length - 1;

                if (levelId < 0 || waveId < 0) return config.PredefinedRaids[0].Waves[0];

                return config.PredefinedRaids[levelId].Waves[waveId];
            }
        }

        private bool IsLastWaveOfLevel
        {
            get
            {
                int levelId = save.CURRENT_LEVEL - 1;
                if (levelId >= config.PredefinedRaids.Length) levelId = config.PredefinedRaids.Length - 1;

                int waveId = save.CURRENT_WAVE - 1;
                if (waveId > config.PredefinedRaids[levelId].Waves.Length) waveId = config.PredefinedRaids[levelId].Waves.Length - 1;

                if (levelId < 0 || waveId < 0) return false;

                return waveId >= config.PredefinedRaids[levelId].Waves.Length - 1;
            }
        }

        private Vector3 NextEventPosition
        {
            get
            {
                if (!save.IsInitialized()) return Vector3.zero;

                if (save.CURRENT_LEVEL != _nextEventLocationLevel)
                {
                    _nextEventLocation = CalculateNextEventPosition();
                    _nextEventLocationLevel = save.CURRENT_LEVEL;
                }

                return _nextEventLocation;
            }
        }

        private Vector3 CalculateNextEventPosition()
        {
            // Game flow:
            // 60 minutes prep time
            // 1. Attack Closest Eikthyr to spawn point (eikthyr stone)
            // 120 minutes prep time
            // 2. Closest Elder to previous
            // 180 minutes prep time
            // 3. Closest bonemass to previous
            // 240 minutes prep time
            // 4. Closest dragon to previous
            // 240 minutes prep time
            // 5. Closest goblin to previous

            Vector3 pos = Vector3.zero;
            if (save.CURRENT_LEVEL == 0) return pos;

            pos = GetClosestLocationOfType(LocationType.EikthyrAltar, pos).Position;
            if (save.CURRENT_LEVEL == 1) return pos;

            pos = GetClosestLocationOfType(LocationType.ElderAltar, pos).Position;
            if (save.CURRENT_LEVEL == 2) return pos;

            pos = GetClosestLocationOfType(LocationType.BonemassAltar, pos).Position;
            if (save.CURRENT_LEVEL == 3) return pos;

            pos = GetClosestLocationOfType(LocationType.ModerAltar, pos).Position;
            if (save.CURRENT_LEVEL == 4) return pos;

            pos = GetClosestLocationOfType(LocationType.YagluthAltar, pos).Position;
            if (save.CURRENT_LEVEL == 5) return pos;

            return Vector3.zero;
        }

        private Location GetClosestLocationOfType(LocationType type, Vector3 to)
        {
            float min = float.MaxValue;
            Location result = null;

            foreach (Location l in RelevantLocations)
            {
                if (l.Type == type)
                {
                    if ((to - l.Position).sqrMagnitude < min)
                    {
                        min = (to - l.Position).sqrMagnitude;
                        result = l;
                    }
                }
            }

            return result;
        }

        private int SecondsLeft
        {
            get
            {
                return save.NEXT_EVENT_TIME;
            }
        }

        private void DebugCloseToPlayerObjects()
        {
            foreach (ZDO zdo in ZDOByID.Values)
            {
                foreach (Player p in Player.GetAllPlayers())
                {

                    if (Vector3.Distance(p.transform.position, zdo.GetPosition()) < 10f)
                    {
                        Chat.instance.SendText(Talker.Type.Normal, p.transform.position + " " + zdo.GetPrefab() + " " + zdo.GetPosition());
                        Debug.Log(zdo.GetPrefab() + " " + zdo.GetPosition());
                    }
                }
            }
        }

        private void InitializeNewGame()
        {
            save.CURRENT_WAVE = 1;
            save.CURRENT_LEVEL = 1;
            save.IS_FIGHTING = false;
            save.NEXT_EVENT_TIME = config.GetSecondsToLevel(1);
        }

        private void ExploreAllMap_Client()
        {
            if (Minimap.instance != null)
            {
                Minimap.instance.ExploreAll();
            }
        }

        private void SetAreaPin_Client(string name, Vector3 position)
        {
            if (Minimap.instance == null) return;

            // Check if we already have such a pin
            foreach (Minimap.PinData pin in MinimapPins)
            {
                if (pin.m_type == Minimap.PinType.EventArea && Mathf.Abs(position.x - pin.m_pos.x) < 1f && Mathf.Abs(position.z - pin.m_pos.z) < 1f)
                {
                    pin.m_name = name;
                    return;
                }
            }
            Minimap.instance.AddPin(position, Minimap.PinType.EventArea, name, false, false);
        }

        public void Save(string path)
        {
            save.Save(path);
        }

        public void Load(string path)
        {
            save.Load(path);
            LoadConfig(save);
            ResetDataBeforeGameLoad();
            cllcLoaded = CreatureLevelControl.API.IsLoaded();
        }

        private void LoadConfig(SaveData save)
        {
            config = new Config();

            if (save.CONFIG_NAME.Equals("default"))
            {
                Debug.Log("Loaded default config");
                SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Loaded default config");
                return;
            }

            // Load config contents
            string rawData = "";
            try
            {
                rawData = File.ReadAllText(Patcher.PluginPath + "/" + save.CONFIG_NAME + CONFIG_FILE_EXTENSION);
            }
            catch
            {
                Debug.LogError("Could not find config file " + save.CONFIG_NAME + CONFIG_FILE_EXTENSION);
                SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS",
                    "Could not find config file " + save.CONFIG_NAME + CONFIG_FILE_EXTENSION);
                return;
            }

            try
            {
                Config c = fastJSON.JSON.ToObject<Config>(rawData);
                config = c;
                Debug.Log("Loaded config " + save.CONFIG_NAME);
                SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS", "Loaded config " + save.CONFIG_NAME);
            }
            catch (Exception ex)
            {
                Debug.LogError("Failed to deserialize config file " + save.CONFIG_NAME + CONFIG_FILE_EXTENSION + ": " + ex.Message);
                SendChatMessage(Vector3.zero, Talker.Type.Shout, "RAIDS",
                    "Failed to deserialize config file " + save.CONFIG_NAME + CONFIG_FILE_EXTENSION + ": " + ex.Message);
                return;
            }
        }

        private void ResetDataBeforeGameLoad()
        {
            _relevantLocations.Clear();
            _nextEventLocationLevel = -1000;
        }

        public List<Location> RelevantLocations
        {
            get
            {
                if (_relevantLocations.Count == 0)
                {
                    foreach (ZoneSystem.LocationInstance i in ZoneSystem.instance.GetLocationList())
                    {
                        switch (i.m_location.m_hash)
                        {
                            case BOSS_ZONE_EIKTHYR:
                                _relevantLocations.Add(new Location { Position = i.m_position, Type = LocationType.EikthyrAltar });
                                break;
                            case BOSS_ZONE_ELDER:
                                _relevantLocations.Add(new Location { Position = i.m_position, Type = LocationType.ElderAltar });
                                break;
                            case BOSS_ZONE_BONEMASS:
                                _relevantLocations.Add(new Location { Position = i.m_position, Type = LocationType.BonemassAltar });
                                break;
                            case BOSS_ZONE_DRAGONQUEEN:
                                _relevantLocations.Add(new Location { Position = i.m_position, Type = LocationType.ModerAltar });
                                break;
                            case BOSS_ZONE_YAGLUTH:
                                _relevantLocations.Add(new Location { Position = i.m_position, Type = LocationType.YagluthAltar });
                                break;
                        }
                    }
                }

                return _relevantLocations;
            }
        }
    }
}
