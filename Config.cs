﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaidDefense
{
    [Serializable]
    public class Config
    {
        public float SpawnInnerRadius = 40f;
        public float SpawnOuterRadius = 60f;

        public int WaveCooldown = 10;
        public int LoseLevelCooldown = 60 * 15;
        public int GameCharacterSoftCap = 100;

        public int[] FullSecondsToLevelStart = new int[] { 60 * 30, 60 * 120, 60 * 180 };

        public int GetSecondsToLevel(int level)
        {
            int levelIndex = level - 1;

            if (levelIndex < 0) return FullSecondsToLevelStart[0];

            if (levelIndex >= FullSecondsToLevelStart.Length)
                return FullSecondsToLevelStart[FullSecondsToLevelStart.Length - 1];

            return FullSecondsToLevelStart[levelIndex];
        }

        public EnemyRaid[] PredefinedRaids = new EnemyRaid[]
        {
            // Eikthyr Raid
            new EnemyRaid
            {
                Waves = new EnemyWave[]
                {
                    new EnemyWave
                    {
                        durationSeconds = 120,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Boar, stars = 2, count = 8 },
                        goalUnitSpawnDeltaSeconds = 4,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Neck, stars = 0, count = 2 },
                        },
                        helperWaveSpawnDeltaSeconds = 30
                    },

                    new EnemyWave
                    {
                        durationSeconds = 3 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Greyling, stars = 1, count = 12 },
                        goalUnitSpawnDeltaSeconds = 4,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Boar, stars = 0, count = 4 }
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 4 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Greyling, stars = 2, count = 12 },
                        goalUnitSpawnDeltaSeconds = 4,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Boar, stars = 0, count = 4 }
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 8 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Eikthyr, stars = 0, count = 1  },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greyling, stars = 0, count = 4 }
                        },
                        helperWaveSpawnDeltaSeconds = 60
                    }
                }
            },

            // Elder Raid
            new EnemyRaid
            {
                Waves = new EnemyWave[]
                {
                    new EnemyWave
                    {
                        durationSeconds = 4 * 60,
                        goalUnitSpawnDeltaSeconds = 30,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.GreydwarfBrute, stars = 2, count = 5  },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greydwarf, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greydwarf, stars = 1, count = 1 },
                        },
                        helperWaveSpawnDeltaSeconds = 60
                    },

                    new EnemyWave
                    {
                        durationSeconds = 5 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Troll, stars = 0, count = 3  },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greyling, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greydwarf, stars = 0, count = 1 },
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    },

                    new EnemyWave
                    {
                        durationSeconds = 4 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.RancidRemains, stars = 2, count = 5  },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Skeleton, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Ghost, stars = 1, count = 1 },
                        },
                        helperWaveSpawnDeltaSeconds = 60
                    },

                    new EnemyWave
                    {
                        durationSeconds = 5 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Troll, stars = 2, count = 1  },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greyling, stars = 1, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greydwarf, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.GreydwarfBrute, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.GreydwarfShaman, stars = 1, count = 1 },
                        },
                        helperWaveSpawnDeltaSeconds = 180
                    },

                    new EnemyWave
                    {
                        durationSeconds = 15 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Elder, stars = 0, count = 1 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Greydwarf, stars = 0, count = 5 }
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    }
                }
            },

            // Bonemass Raid
            new EnemyRaid
            {
                Waves = new EnemyWave[]
                {
                    new EnemyWave
                    {
                        durationSeconds = 4 * 60,
                        goalUnitSpawnDeltaSeconds = 20,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Draugr, stars = 2, count = 6 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Skeleton, stars = 0, count = 4 }
                        },
                        helperWaveSpawnDeltaSeconds = 60
                    },

                    new EnemyWave
                    {
                        durationSeconds = 5 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Oozer, stars = 2, count = 6 },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Surtling, stars = 2, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Blob, stars = 0, count = 2 },
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 5 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Wraith, stars = 2, count = 8 },
                        goalUnitSpawnDeltaSeconds = 20,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Surtling, stars = 1, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Draugr, stars = 1, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Blob, stars = 0, count = 2 },
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    },

                    new EnemyWave
                    {
                        durationSeconds = 5 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.DraugrElite, stars = 2, count = 5 },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Draugr, stars = 0, count = 8 }
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 20 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Bonemass, stars = 0, count = 1 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Surtling, stars = 2, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Draugr, stars = 1, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Blob, stars = 0, count = 1 },
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    }
                }
            },

            // Moder Raid
            new EnemyRaid
            {
                Waves = new EnemyWave[]
                {
                    new EnemyWave
                    {
                        durationSeconds = 4 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Wolf, stars = 2, count = 3 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Drake, stars = 2, count = 1 }
                        },
                        helperWaveSpawnDeltaSeconds = 60
                    },

                    new EnemyWave
                    {
                        durationSeconds = 8 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Fenring, stars = 2, count = 4 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Drake, stars = 0, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Wolf, stars = 0, count = 2 },
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    },

                    new EnemyWave
                    {
                        durationSeconds = 15 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.StoneGolem, stars = 2, count = 1 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Drake, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Wolf, stars = 0, count = 2 },
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 20 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Moder, stars = 0, count = 1 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Drake, stars = 2, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Wolf, stars = 1, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Wolf, stars = 0, count = 2 }
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    }
                }
            },

            // Yagluth Raid
            new EnemyRaid
            {
                Waves = new EnemyWave[]
                {
                    new EnemyWave
                    {
                        durationSeconds = 6 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Lox, stars = 2, count = 3 },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Deathsquito, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSword, stars = 0, count = 3 }
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    },

                    new EnemyWave
                    {
                        durationSeconds = 10 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSpear, stars = 2, count = 6 },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Deathsquito, stars = 0, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSword, stars = 0, count = 2 }
                        },
                        helperWaveSpawnDeltaSeconds = 90
                    },

                    new EnemyWave
                    {
                        durationSeconds = 6 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingBerseker, stars = 2, count = 3 },
                        goalUnitSpawnDeltaSeconds = 30,
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSword, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSpear, stars = 0, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingShaman, stars = 0, count = 1 }
                        },
                        helperWaveSpawnDeltaSeconds = 120
                    },

                    new EnemyWave
                    {
                        durationSeconds = 20 * 60,
                        goalGroup = new EnemyGroup { enemy = EnemyUtil.EnemyType.Yagluth, stars = 0, count = 1 },
                        helperGroups = new EnemyGroup[]
                        {
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.Deathsquito, stars = 1, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSpear, stars = 2, count = 1 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSword, stars = 0, count = 2 },
                            new EnemyGroup { enemy = EnemyUtil.EnemyType.FulingSword, stars = 1, count = 1 }
                        },
                        helperWaveSpawnDeltaSeconds = 180
                    }
                }
            },
        };
    }
}
