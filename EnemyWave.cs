﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace RaidDefense
{
    [Serializable]
    public class EnemyWave
    {
        public int durationSeconds = 120;

        public EnemyGroup goalGroup = new EnemyGroup();
        public int goalUnitSpawnDeltaSeconds = 10;

        public EnemyGroup[] helperGroups = new EnemyGroup[0];
        public int helperWaveSpawnDeltaSeconds = 60;
    }
}
