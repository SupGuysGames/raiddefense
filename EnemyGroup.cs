﻿using CreatureLevelControl;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RaidDefense
{
    [Serializable]
    public class EnemyGroup
    {
        public EnemyUtil.EnemyType enemy = EnemyUtil.EnemyType.Boar;
        public int stars = 0;
        public int count = 1;
        public BossAffix bossAffix = BossAffix.None;
        public CreatureExtraEffect extraEffect = CreatureExtraEffect.None;
        public CreatureInfusion creatureInfusion = CreatureInfusion.None;
    }
}
